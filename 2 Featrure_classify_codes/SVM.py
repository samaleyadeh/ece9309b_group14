#import necessary libiraries
import sklearn.svm as ssv  
import joblib  
import glob  
import os  
import time
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
# difine model path and trained feat path
model_path = './svm.model'  

train_feat_path = './features/train/'  
fds = []  
labels = []  
num=0  
for feat_path in glob.glob(os.path.join(train_feat_path, '*.feat')):  
    num += 1
    data = joblib.load(feat_path)  
    fds.append(data[:-1])  
    labels.append(data[-1])  
#PCA to reduce dimension
pca_c=PCA(n_components=200,copy=True)
# tried components from 100 to 1000 and the 200 shows the best result
fds=pca_c.fit_transform(fds)
joblib.dump(pca_c,'./pca.model')

t0 = time.time()  
print(fds.shape)


parameter_grid = [  {'kernel': ['linear'], 'C': [1, 10, 50, 500, 1000]},
                    {'kernel': ['poly'], 'degree': [2, 3, 4, 5]},
                    {'kernel': ['rbf'], 'gamma': [0.1, 0.01, 0.001], 'C': [1, 10, 50, 500, 1000]},
                 ]
#------------------------SVM--------------------------------------------------  
# train the SVM classifier
#clf = ssv.SVC(kernel='rbf',C=10,gamma=0.1)
clf = ssv.SVC(kernel='rbf')  #tune:poly, linear
#clf = ssv.SVC(kernel='linear',C=600
#clf = GridSearchCV(ssv.SVC(C=1), parameter_grid, cv = 5, scoring = 'accuracy',iid = False)

#Highest scoring parameter set: {'kernel': 'rbf', 'gamma': 0.1, 'C': 10}

print("Training a SVM Classifier.")  
clf.fit(fds, labels)  
joblib.dump(clf, model_path)
'''
print ("\nHighest scoring parameter set:", clf.best_params_)
print ("\nHighest accuracy:", clf.best_score_)
#'''
#------------------------SVM--------------------------------------------------  
t1 = time.time()  
print("Classifier saved to {}".format(model_path))   
print('The training time is :%f seconds' % (t1-t0))
#clf = clf.best_estimator_
y_pred=clf.predict(X=fds)
t2 = time.time()
#print testing time and train accuracy  
print('The testing time is :%f seconds' % (t2-t1))
print('Accuracy on train set:',accuracy_score(y_true=labels,y_pred=y_pred))

# Test set
# difine the test feat path
test_feat_path = './features/test/'  
fds = []  
labels = []  
num=0
#get stored features  
for feat_path in glob.glob(os.path.join(test_feat_path, '*.feat')):  
    num += 1
    data = joblib.load(feat_path)  
    fds.append(data[:-1])  
    labels.append(data[-1]) 
# call  PCA to reduce computing amount and select most important features
pca_c=joblib.load('./pca.model')
fds=pca_c.transform(fds)
# call the SVM to classify the trained features
clf=joblib.load('./svm.model')
t3 = time.time()  
y_pred=clf.predict(X=fds)
t4 = time.time()  
# print results
print('The testing time is :%f seconds' % (t4-t3))
print('Accuracy on test set:',accuracy_score(y_true=labels,y_pred=y_pred))
