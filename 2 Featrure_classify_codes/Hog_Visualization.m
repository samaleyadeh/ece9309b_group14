clear all; close all; clc;    

image=double(imread('1.jpg'));    
imshow(image,[]);
[m, n]=size(image);

image=sqrt(image);  % Gamma correction
Y=[-1 0 1];        % Define vertical template
X=Y';             % Define horizontal template
By=imfilter(image,Y,'replicate');    % Vertical edges
Bx=imfilter(image,X,'replicate');    % Horiizon edges
Ied=sqrt(Bx.^2+By.^2);              % Edge insensity
Iphase=By./Bx;              % Edge slope
% Difine cells
cell=18;                % step*step as a cell
orientation=9;               % number of orientation
jiao=360/orientation;        % angle of per orientation
Cell=cell(1,1);              % cell can be add, set as 1
A=1;                      
B=1;
for i=1:cell:m          % If m/step is not intger,'i=1:step:m-step' format is the best
    A=1;
    for a=1:cell:n      
        M=Bx(i:i+cell-1,a:a+cell-1);
        N=Ied(i:i+cell-1,a:a+cell-1);
        N=N/sum(sum(N));        % Local edge strength normalization
        tmpphase=Iphase(i:i+cell-1,a:a+cell-1);
        H=zeros(1,orientation);               
        for i=1:cell
            for j=1:cell
                if isnan(tmpphase(i,j))==1  % if pixel is nan, reset as 0
                    tmpphase(i,j)=0;
                end
                AN=atan(tmpphase(i,j));    % atan focus on dgree [-90 90]
                AN=mod(AN*180/pi,360);    % transfer -90 to 270
                if M(i,j)<0              %according to x, determine real angle
                    if AN<90               % if in the first quadrant
                        AN=AN+180;        % move to the third quadrant
                    end
                    if AN>270              % if in the fourth quadrant
                        AN=AN-180;        % move to the second quagrant
                    end
                end
                AN=AN+0.0000001;          % make sure ang do not equals 0
                H(ceil(AN/jiao))=H(ceil(AN/jiao))+N(i,j);   % ceil round up
            end
        end
        H=H/sum(H);    %Direction histogram normalization
        Cell{A,B}=H;       
        A=A+1;                
    end
    B=B+1;                    
end

%Get features
[m, n]=size(Cell);
feature=cell(1,(m-1)*(n-1));
for i=1:m-1
   for a=1:n-1           
        f=[];
        f=[f Cell{i,a}(:)' Cell{i,a+1}(:)' Cell{i+1,a}(:)' Cell{i+1,a+1}(:)'];
        feature{(i-1)*(n-1)+a}=f;
   end
end

%Generate the image
l=length(feature);
f=[];
for i=1:l
    f=[f;feature{i}(:)'];  
end 
figure
mesh(f)