from skimage.feature import hog
import numpy as np
import os
import joblib
import cv2
from sklearn.preprocessing import  LabelEncoder

#convert to grayscale images
def rgb2gray(im):
    gray = im[:, :, 0]*0.2989+im[:, :, 1]*0.5870+im[:, :, 2]*0.1140
    return gray
	
def get_label_encoder():
    labels_origin=[]
    for i in os.listdir('./train96/'):
        labels_origin.append(i)
    le=LabelEncoder()
    le.fit(labels_origin)
    return le

#get all the images
def getData_Label(filePath):
    dataset = []
    labels = []
    for childir in os.listdir(filePath):
        for img in os.listdir(os.path.join(filePath, childir)):
            f = os.path.join(filePath, childir, img)
            data = cv2.imread(f)
            data = cv2.resize(data, (96, 96), interpolation=cv2.INTER_CUBIC)
            data = np.reshape(data, (96 * 96, 3))
            data.shape = 1, 3, -1
            label = childir
            dataset.append(data)
            labels.append([label])
    return dataset, labels

# define some parameters
normalize = True
visualize = False
block_norm = 'L2-Hys'
cells_per_block = [2, 2]  # number of cells per block
pixels_per_cell = [8, 8]  # number of pixels per cell
orientations = 9  # number of orientations
def getFeat(datasets,labels,mode):
    num = 0
    le=get_label_encoder()
    labels=le.transform(labels)
    labels=labels.reshape((labels.shape[0],1))
    for i,data in enumerate(datasets):
        image = np.reshape(data, (96, 96, 3))
        gray = rgb2gray(image) / 255.0  # trans image to gray
        fd = hog(gray, orientations, pixels_per_cell, cells_per_block, # call Hog Algorithm to extrat all features
                 block_norm, visualize)
        fd=np.concatenate((fd,labels[i]))
        fd_name = str(num+1) + '.feat'  # set file name
        if mode == 'train':
            if not os.path.exists('./features/train/'):
                os.makedirs('./features/train/')
            fd_path = os.path.join('./features/train/', fd_name)
        else:
            if not os.path.exists('./features/test/'):
                os.makedirs('./features/test/')
            fd_path = os.path.join('./features/test/', fd_name)
        joblib.dump(fd, fd_path, compress=3)  # save data to local
        num += 1
    print('Finish feature extraction')

#Save the train data
filepath='./train96/'
mode='train'
datasets,labels=getData_Label(filePath=filepath)
getFeat(datasets=datasets,labels=labels,mode=mode)

filepath='./val96/'
mode='test'
datasets,labels=getData_Label(filePath=filepath)
getFeat(datasets=datasets,labels=labels,mode=mode)