Data=rgb2gray(imread('1.jpg')); %read the data
N=size(Data,1); % the number of rows
M=size(Data,2); % the number of column
T=uint8(zeros(N,M)); %set the neighbor pixels

for a=2:1:N-1
    for b=2:1:M-1
        SUR=uint8(zeros(1,8)); %initializaiton
        % get the value of surrounding pixels
        SUR(1,1)=Data(a-1,b);
        SUR(1,2)=Data(a-1,b+1);
        SUR(1,3)=Data(a,b+1);
        SUR(1,4)=Data(a+1,b+1);
        SUR(1,5)=Data(a+1,b);
        SUR(1,6)=Data(a+1,b-1);
        SUR(1,7)=Data(a,b-1);
        SUR(1,8)=Data(a-1,b-1);
        CEN=Data(a,b);
        Z=uint8(0);
        for s=1:1:8
             Z =Z+ (SUR(1,s) >= CEN)* 2^(s-1);
        end
        T(a,b)=Z;
       
    end
end
%show images
subplot(1,2,2);
imshow(T);
title('After LBP');
subplot(1,2,1);
imshow('1.jpg');
title('Original Picture');

