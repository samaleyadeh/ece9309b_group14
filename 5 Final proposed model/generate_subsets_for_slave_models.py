

import os
import numpy as np
import cv2
from keras.preprocessing.image import  ImageDataGenerator
from keras.models import Model,load_model,Sequential
import keras
from keras.preprocessing.image import load_img,img_to_array
import math
import random
from keras.utils import plot_model
import matplotlib.pyplot as plt



TARGET_SIZE=(224,224)
INPUT_SIZE=(224,224,3)
BATCHSIZE=16 
train_datagen = ImageDataGenerator(rescale=1./255)

test_datagen = ImageDataGenerator(rescale=1./255)

val_datagen = ImageDataGenerator(rescale=1./255)



#generate images from train set, test set and validation set
train_generator = train_datagen.flow_from_directory(
        './train/',
        target_size=TARGET_SIZE,
        batch_size=BATCHSIZE,
        class_mode='categorical')
test_generator = test_datagen.flow_from_directory(
        './test/',
        target_size=TARGET_SIZE,
        batch_size=BATCHSIZE,
        class_mode='categorical')
val_generator = val_datagen.flow_from_directory(
        './val/',
        target_size=TARGET_SIZE,
        batch_size=BATCHSIZE,
        class_mode='categorical')



#generate labels indicating cancer (1) or not cancer (0)
label=train_generator.class_indices
label={v:k for k,v in label.items()}
print(label)


#load trained resnet model as the master model
model1=load_model('./resnet.h5')



array_of_img = []
a= []
len=0
len1=0
directory_name="./train/1/" 
for filename in os.listdir(r"./"+directory_name): 
    test = cv2.imread(directory_name + filename)
    test=test/255.
    test_shape=(1,)+test.shape
    test=test.reshape(test_shape)
    res=model1.predict(test) #use master model to test on the validation set
    prob=res[0,np.argmax(res,axis=1)[0]] #generate the confidence list of all images in the validation set
    res=label[np.argmax(res,axis=1)[0]] #generate the predicted label list of all images in the validation set
    if res!="1": #if false, add the image name to the false negative name list
        array_of_img.append(filename)
        len=len+1

print(len) #print the number of false negatives


#read the name list and copy all the false negatives to a new folder
import shutil
for filename in os.listdir(r"./"+directory_name):
    if filename in array_of_img:
        srcfile=r"./"+directory_name+filename
        dstfile=r"./train_false/1/"+filename
        shutil.copyfile(srcfile,dstfile)


array_of_img = []
a= []
len=0
len1=0
directory_name="./train/0/" 
for filename in os.listdir(r"./"+directory_name): 
    test = cv2.imread(directory_name + filename)
    test=test/255.
    test_shape=(1,)+test.shape
    test=test.reshape(test_shape)
    res=model1.predict(test) #use master model to test on the validation set
    prob=res[0,np.argmax(res,axis=1)[0]] #generate the confidence list of all images in the validation set
    res=label[np.argmax(res,axis=1)[0]] #generate the predicted label list of all images in the validation set
    if res!="0": #if false, add the image name to the false positives name list
        array_of_img.append(filename)
        len=len+1

print(len) #print the number of false positives


import shutil
#read the name list and copy all the false positives to a new folder
for filename in os.listdir(r"./"+directory_name2):
    if filename in array_of_img:
        srcfile=r"./"+directory_name2+filename
        dstfile=r"./train_false/0/"+filename
        shutil.copyfile(srcfile,dstfile)

