
# coding: utf-8


import os
import numpy as np
import cv2
from keras.preprocessing.image import  ImageDataGenerator
from keras.models import Model,load_model,Sequential
import keras
from keras.preprocessing.image import load_img,img_to_array
import math
import random
from keras.utils import plot_model
import matplotlib.pyplot as plt



TARGET_SIZE=(224,224)
INPUT_SIZE=(224,224,3)
BATCHSIZE=16
train_datagen = ImageDataGenerator(rescale=1./255)

test_datagen = ImageDataGenerator(rescale=1./255)

val_datagen = ImageDataGenerator(rescale=1./255)



#generate images from test set and validation set
test_generator = test_datagen.flow_from_directory(
        './test/',
        target_size=TARGET_SIZE,
        batch_size=BATCHSIZE,
        class_mode='categorical')

val_generator = val_datagen.flow_from_directory(
        './val/',
        target_size=TARGET_SIZE,
        batch_size=BATCHSIZE,
        class_mode='categorical')



label=test_generator.class_indices
label={v:k for k,v in label.items()}
print(label)



model1=load_model('./resnet.h5') #load master model
model2=load_model('./xception.h5') #load slave model 1 trained on false negatives
model3=load_model('./vgg.h5') #load slave model 2 trained on false positives



#read images from validation folder
import os
rootdir = './val/'
test_laels = []
test_images=[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        if not file.endswith(".jpg"):
            continue
        test_laels.append(subdir.split('/')[-1])
        test_images.append(os.path.join(subdir, file))
        
print(test_laels[0],test_images[0])



import time
from PIL import Image
predict=[]
model1_all=[]
model1_proball=[]
length=len(test_images)
diff=0
count=0
count1=0
t1 = time.time()
for i in range(length):
    inputimg=test_images[i]
    #print(inputimg)
    test_batch=[]
    thisimg=np.array(Image.open(inputimg))/255 #read all the images in validation set
    #print(thisimg)
    test_shape=(1,)+thisimg.shape
    thisimg=thisimg.reshape(test_shape)
    model1_batch=model1.predict(thisimg) #use master model to process the input image
    #print(model1_batch)
    prob=model1_batch[0,np.argmax(model1_batch,axis=1)[0]] #generate the confidence list of all images in the validation set
    res=label[np.argmax(model1_batch,axis=1)[0]] #generate the predicted label list of all images in the validation set
    #print(prob)
    #print(res)
    
    #pass through the slave models only when low confidence (<0.9)
    if (res=="0")&(prob<0.9): #for negatives decided by the master model, test on slave model 1 to reduce false negatives
        res2=model2.predict(thisimg)
        res2=label[np.argmax(res2,axis=1)[0]]
        #print(res2)
        if res!=res2: #if slave model 1 doesn't agree, means it might be a false negative, so overwrite the result
            res=res2        
        diff=diff+1
    if (res=="1")&(prob<0.9): #for positives decided by the master model, test on slave model 2 to reduce false positives
        res3=model3.predict(thisimg)
        res3=label[np.argmax(res3,axis=1)[0]]
        #print(res2)
        if res!=res3:  #if slave model 2 doesn't agree, means it might be a false positive, so overwrite the result
            res=res3        
        diff=diff+1    
    if res!=test_laels[i]: #count the false images
        count=count+1  
t2 = time.time()
acc=(length-count)/length
print("accuracy:%f"%acc)
print('The testing time is :%f seconds' % (t2-t1))




