import os
import cv2
from keras.preprocessing.image import  ImageDataGenerator
from keras.models import Model,load_model,Sequential
import keras
from keras.preprocessing.image import load_img,img_to_array
import math
import random
from keras.utils import plot_model

TARGET_SIZE=(224,224)
INPUT_SIZE=(224,224,3)
BATCHSIZE=16

test_datagen = ImageDataGenerator(rescale=1./255)

#generate validation set
vali_generator = test_datagen.flow_from_directory(
        './val/',
        target_size=TARGET_SIZE,
        batch_size=BATCHSIZE,
        class_mode='categorical')
		
model1=load_model('./xception.h5')	#load trained xception model
score = model1.evaluate_generator(vali_generator, 1000)
print("Accuracy1 = ",score)	#0.9453
model2=load_model('./vgg.h5')	#load trained vgg16 model
score = model2.evaluate_generator(vali_generator, 1000)
print("Accuracy2 = ",score) #0.9399
model3=load_model('./resnet.h5')	#load trained resnet model
score = model3.evaluate_generator(vali_generator, 1000)
print("Accuracy3 = ",score) #0.9555