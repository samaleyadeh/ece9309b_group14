import cv2
import matplotlib.pyplot as plt
import os, csv
import numpy as np
import itertools
from collections import OrderedDict
from scipy import misc
from collections import Iterable
import random
import shutil

#resize the images to 224*224
def get_224(folder,dstdir):
    imgfilepaths=[]
    for root,dirs,imgs in os.walk(folder):
        for thisimg in imgs:
            thisimg_path=os.path.join(root,thisimg)
            imgfilepaths.append(thisimg_path)
    for thisimg_path in imgfilepaths:
        dir_name,filename=os.path.split(thisimg_path)
        dir_name=dir_name.replace(folder,dstdir)
        new_file_path=os.path.join(dir_name,filename)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        img=cv2.imread(thisimg_path)
        img=cv2.resize(img,(224,224))
        cv2.imwrite(new_file_path,img)
    print('Finish resizing'.format(folder=folder))

DATA_DIR_224='./original/'	#resize the images in the original dataset
get_224(folder='./train/',dstdir=DATA_DIR_224)

Numbers=16000	#size of test set and validation set
Train_Dir='./train/'
Dest_Dir='./test/'
Val_Dir='./val/'
def mymovefile(srcfile,dstfile):	# move file from one folder to another
    if not os.path.isfile(srcfile):
        print ("%s not exist!"%(srcfile))
    else:
        fpath,fname=os.path.split(dstfile)    
        if not os.path.exists(fpath):
            os.makedirs(fpath)               
        shutil.move(srcfile,dstfile)          
        print ("move %s -> %s"%(srcfile,dstfile))
allimgs=[]
for subdir in os.listdir(Train_Dir):
    for filename in os.listdir(os.path.join(Train_Dir,subdir)):
        filepath=os.path.join(Train_Dir,subdir,filename)
        allimgs.append(filepath)
print(len(allimgs))

test_imgs=random.sample(allimgs,Numbers)
for img in test_imgs:
    dest_path=img.replace(Train_Dir,Dest_Dir)	#randomly selected 16000 image from train set to test set
    mymovefile(img,dest_path)
print('Finish creating test set')

val_imgs=random.sample(allimgs,Numbers)
for img in val_imgs:
    dest_path=img.replace(Train_Dir,Val_Dir)	#randomly selected 16000 image from train set to validation set
    mymovefile(img,dest_path)
print('Finish creating validation set')
