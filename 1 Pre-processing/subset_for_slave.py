import cv2
import matplotlib.pyplot as plt
import os, csv
import numpy as np
import itertools
from collections import OrderedDict
from scipy import misc
from collections import Iterable
import random
import shutil

Train_Dir='./FT/'
Dest_Dir='./FT_test/'	#generate the test set of bias subset for FPs
def mymovefile(srcfile,dstfile):
    if not os.path.isfile(srcfile):
        print ("%s not exist!"%(srcfile))
    else:
        fpath,fname=os.path.split(dstfile)    
        if not os.path.exists(fpath):
            os.makedirs(fpath)               
        shutil.move(srcfile,dstfile)          
        print ("move %s -> %s"%(srcfile,dstfile))
allimgs=[]
for subdir in os.listdir(Train_Dir):
    for filename in os.listdir(os.path.join(Train_Dir,subdir)):
        filepath=os.path.join(Train_Dir,subdir,filename)
        allimgs.append(filepath)
print(len(allimgs))

test_imgs=random.sample(allimgs,2000) #size of test set
for img in test_imgs:
    dest_path=img.replace(Train_Dir,Dest_Dir)
    mymovefile(img,dest_path)
print('Finish creating test set')

Train_Dir='./FN/'
Dest_Dir='./FN_test/'	#generate the test set of bias subset for FNs

for subdir in os.listdir(Train_Dir):
    for filename in os.listdir(os.path.join(Train_Dir,subdir)):
        filepath=os.path.join(Train_Dir,subdir,filename)
        allimgs.append(filepath)
print(len(allimgs))

test_imgs=random.sample(allimgs,2000) #size of test set
for img in test_imgs:
    dest_path=img.replace(Train_Dir,Dest_Dir)
    mymovefile(img,dest_path)
print('Finish creating test set')